package com.example.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class IndieU_Tests {

	private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();

	  @Before
	  public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
	    driver = new ChromeDriver();
	    baseUrl = "http://indieu.ddns.net:8080/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }

	  @Test
	  public void LoginTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/");
	    driver.findElement(By.linkText("Login/Sign Up")).click();
	    driver.findElement(By.name("username")).clear();
	    driver.findElement(By.name("username")).sendKeys("cool");
	    driver.findElement(By.id("password")).clear();
	    driver.findElement(By.id("password")).sendKeys("password");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void LogoutTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/");
	    login();
	    Thread.sleep(5000);
	    driver.findElement(By.name("logout")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void SearchBarTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("game");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void SearchFilterTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath("//*[@id='game']")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("//*[@id='profile']")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void BrowseToGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Browse")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("THE Game")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void BrowseToProfileTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Browse")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("Cooler")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void SearchToProfileTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("cool");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("Cooler")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void SearchToGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void DownloadGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("button.download")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void ToDevelopersProfileTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("Screen name double OG")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void GameDonationLinkTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.linkText("Donate to developer!")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void ReviewGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath("//*[@id='rev']")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("/html/body/div[2]/div[6]/form/input[1]")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/div[6]/form/input[1]")).sendKeys("Auto Review");
	    driver.findElement(By.xpath("//*[@id='score']")).click();
	    driver.findElement(By.xpath("/html/body/div[2]/div[6]/form/textarea")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/div[6]/form/textarea")).sendKeys("This is an automated review.");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	    assertEquals("Submitted!", closeAlertAndGetItsText());
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void ReportBugTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath("//*[@id='rprt']")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("/html/body/div[2]/form[2]/div/input[1]")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/form[2]/div/input[1]")).sendKeys("Auto Bug Report");
	    driver.findElement(By.xpath("/html/body/div[2]/form[2]/div/textarea")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/form[2]/div/textarea")).sendKeys("This is an automated bug report.");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("div > input.logout")).click();
	    assertEquals("Submitted!", closeAlertAndGetItsText());
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void ReportGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.linkText("Search")).click();
	    driver.findElement(By.name("SearchString")).clear();
	    driver.findElement(By.name("SearchString")).sendKeys("welcome");
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    driver.findElement(By.linkText("welcome to the chat")).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath("//*[@id='bug']")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("/html/body/div[2]/form[3]/div/input[1]")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/form[3]/div/input[1]")).sendKeys("Auto Report");
	    driver.findElement(By.xpath("/html/body/div[2]/form[3]/div/textarea")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/form[3]/div/textarea")).sendKeys("This is an automated report.");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > div > input.logout")).click();
	    assertEquals("Submitted!", closeAlertAndGetItsText());
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void UpdateProfileTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.xpath("//*[@id='header']/ul/li[5]/a")).click();
	    driver.findElement(By.name("newsname")).clear();
	    driver.findElement(By.name("newsname")).sendKeys("");
	    driver.findElement(By.name("newsname")).clear();
	    driver.findElement(By.name("newsname")).sendKeys("Cooler");
	    driver.findElement(By.name("userdesc")).clear();
	    driver.findElement(By.name("userdesc")).sendKeys("");
	    driver.findElement(By.name("userdesc")).clear();
	    driver.findElement(By.name("userdesc")).sendKeys("You can't get any cooler than this");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void FavoriteGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.linkText("My Profile")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.linkText("THE Game")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.name("favorite")).click();
	    Thread.sleep(5000);
	  }
	  
	  @Test
	  public void UpdateGameTest() throws Exception {
	    driver.get(baseUrl + "/indie_u/index.html");
	    login();
	    driver.findElement(By.linkText("My Profile")).click();
	    driver.findElement(By.linkText("THE Game")).click();
	    driver.findElement(By.linkText("Edit This Game")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("/html/body/div[2]/div[2]/form/textarea")).clear();
	    driver.findElement(By.xpath("/html/body/div[2]/div[2]/form/textarea")).sendKeys("THE Game");
	    driver.findElement(By.name("userdesc")).clear();
	    driver.findElement(By.name("userdesc")).sendKeys("The best game ever.");
	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).clear();
	    driver.findElement(By.xpath("(//input[@type='text'])[2]")).sendKeys("www.google.com");
	    Thread.sleep(5000);
	    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
	    assertEquals("Saved!", closeAlertAndGetItsText());
	    Thread.sleep(5000);
	  }

	  @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }
	  
	  private void login() {
		  driver.findElement(By.linkText("Login/Sign Up")).click();
		    driver.findElement(By.name("username")).clear();
		    driver.findElement(By.name("username")).sendKeys("cool");
		    driver.findElement(By.id("password")).clear();
		    driver.findElement(By.id("password")).sendKeys("password");
		    driver.findElement(By.cssSelector("form.ng-valid.ng-dirty > input.logout")).click();
		    try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	
}
