package indie_u.search;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import indie_u.auth.ConnectionHandler;
import indie_u.game.Game;
import indie_u.profile.UserProfile;

@WebServlet("/SearchServlet")
public class SearchServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public SearchServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String requestType = request.getParameter("Type");
		if (requestType.equals("gsearch")) {
			String searched = request.getParameter("searched");

			List<Game> foundgames = searchedGames(searched);
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (Game item : foundgames) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("game_id", item.game_id);
					anotherone.put("title", item.name);
					anotherone.put("description", item.description);
					anotherone.put("publisher", item.publisher_name);
					anotherone.put("url", item.url);
					anotherone.put("id", item.publisher_id);
					array1.put(anotherone);
				}
				gamelist.put("Games", array1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (requestType.equals("psearch")) {
			String searched = request.getParameter("searched");

			List<UserProfile> foundprofiles = searchedProfiles(searched);
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (UserProfile item : foundprofiles) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("username", item.name);
					anotherone.put("biography", item.bio);
					anotherone.put("id", item.id);
					array1.put(anotherone);
				}
				gamelist.put("Profiles", array1);
			} catch (JSONException e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);

	}

	String searchString(String searched) {
		String Searchable = ".*";
		System.out.println(searched);
		String[] keywords = searched.split("\\s+");
		System.out.println(keywords[0]);
		int k = 0;
		while (k < keywords.length) {
			System.out.println(keywords[k]);
			Searchable = Searchable + (keywords[k]);
			Searchable = Searchable + (".*");
			k++;
		}
		Searchable = "\"" + Searchable;
		Searchable = Searchable + ("\";");
		System.out.println(Searchable);
		return Searchable;

	}

	List<Game> searchedGames(String searched) {
		Connection conn = null;
		try {
			List<Game> found = new ArrayList<Game>();
			String Searchable = searchString(searched);
			String sql = "SELECT * FROM Games WHERE title RLIKE " + Searchable;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				int game_Id = rs.getInt("id");
				System.out.println(game_Id);
				String gameName = rs.getString("title");
				String description = rs.getString("description");
				int author = rs.getInt("publisher_id");
				String author_name = getDisplayName(conn, author);
				String url = rs.getString("url");
				Game find = new Game(game_Id, gameName, description, author_name, author, url);
				found.add(find);
			} else {
				System.out.println("No match found that matches search query");
				return new ArrayList<Game>();
			}
			return found;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return new ArrayList<Game>();
	}

	List<UserProfile> searchedProfiles(String searched) {
		Connection conn = null;
		try {
			List<UserProfile> Profiles = new ArrayList<UserProfile>();
			String Searchable = searchString(searched);
			String sql = "SELECT * FROM Profiles WHERE display_name RLIKE " + Searchable;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			while (rs.next()) {
				String username = rs.getString("display_name");
				String bio = rs.getString("biography");
				int id = rs.getInt("account_id");
				UserProfile thing = new UserProfile(id, username, bio);
				Profiles.add(thing);
			}
			return Profiles;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return new ArrayList<UserProfile>();
	}

	String getDisplayName(Connection conn, int accountId) {
		try {

			String sql = "SELECT * FROM Profiles WHERE account_id LIKE " + accountId;
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				System.out.println("Matching game found");
				// return rs.getString("display_name");
				return "new";
			} else {
				System.out.println("No match found for account id " + accountId);
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}
}
