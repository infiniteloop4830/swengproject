package indie_u.profile;

public class UserProfile {

	public int id;
	public String name;
	public String bio;

	public UserProfile(int id, String name, String bio) {
		this.id = id;
		this.name = name;
		this.bio = bio;
	}

}
