package indie_u.profile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import indie_u.auth.ConnectionHandler;
import indie_u.game.BugReport;
import indie_u.game.Game;
import indie_u.game.Review;

@WebServlet("/ProfileServlet")
public class ProfileServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public ProfileServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Get current contents of a profile, does not require user to be logged
		// in
		if (request.getParameter("Id") == null) {
			System.out.println("Invalid Id");
			return;
		}
		int id = Integer.parseInt(request.getParameter("Id"));

		if (request.getParameter("Type") != null && request.getParameter("Type").equals("All")
				&& request.getParameter("Id").equals("0")) {
			List<UserProfile> foundprofiles = getProfiles();

			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (UserProfile item : foundprofiles) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("username", item.name);
					anotherone.put("biography", item.bio);
					anotherone.put("id", item.id);
					array1.put(anotherone);
				}
				gamelist.put("Profiles", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);
			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("AllByPublisher")) {
			List<Game> foundgames = getGamesByPublisher(id);
			JSONObject gamelist = new JSONObject();
			JSONArray array1 = new JSONArray();
			try {
				for (Game item : foundgames) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("game_id", item.game_id);
					anotherone.put("title", item.name);
					anotherone.put("description", item.description);
					anotherone.put("publisher", item.publisher_name);
					anotherone.put("url", item.url);
					anotherone.put("id", item.publisher_id);
					array1.put(anotherone);
				}
				gamelist.put("Games", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("GetFavoriteGames")) {

			List<Game> foundgames = getFavoriteGames(id);
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (Game item : foundgames) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("game_id", item.game_id);
					anotherone.put("title", item.name);
					anotherone.put("description", item.description);
					anotherone.put("publisher", item.publisher_name);
					anotherone.put("url", item.url);
					anotherone.put("id", item.publisher_id);
					array1.put(anotherone);
				}
				gamelist.put("Games", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("GetReviews")) {

			List<Review> reviews = getReviewsByReviewer(id);
			JSONObject reviewList = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (Review item : reviews) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("id", item.id);
					anotherone.put("reviewer_id", item.accountId);
					anotherone.put("reviwer_name", item.reviewerName);
					anotherone.put("game_id", item.gameId);
					anotherone.put("game_name", item.gameName);
					anotherone.put("review_title", item.title);
					anotherone.put("review_details", item.reviewDetails);
					anotherone.put("review_score", item.score);
					array1.put(anotherone);
				}
				reviewList.put("Reviews", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = reviewList.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("GetAllBugReports")) {

			List<BugReport> bugReports = getBugReportsByPublisher(id);
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (BugReport item : bugReports) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("id", item.id);
					anotherone.put("reporter_id", item.reporterId);
					anotherone.put("reporter_name", item.reporterName);
					anotherone.put("game_id", item.gameId);
					anotherone.put("game_name", item.gameName);
					anotherone.put("details", item.bug_info);
					array1.put(anotherone);
				}
				gamelist.put("BugReports", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("GetGameBugReports")) {

			int gameId = Integer.parseInt(request.getParameter("GameId"));
			List<BugReport> bugReports = getBugReportsByGameId(gameId);
			System.out.println("Anything working...?...?");
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (BugReport item : bugReports) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("id", item.id);
					anotherone.put("reporter_id", item.reporterId);
					anotherone.put("reporter_name", item.reporterName);
					anotherone.put("game_id", item.gameId);
					anotherone.put("game_name", item.gameName);
					anotherone.put("details", item.bug_info);
					array1.put(anotherone);
					System.out.println("This report was found: " + anotherone);
				}
				gamelist.put("BugReports", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		}

		UserProfile profile = getProfile(id);

		// Find file with matching idea and set contents to data
		String data = getFileContents("/var/webapp/profile_pics/" + id);

		JSONObject json = new JSONObject();
		try {
			json.put("id", profile.id);
			json.put("name", profile.name);
			json.put("bio", profile.bio);
			json.put("data", data);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String jsonString = json.toString();
		response.setContentType("application/json");
		response.getWriter().write(jsonString);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Called to edit profile tied to active session
		HttpSession session = request.getSession();
		if (session.getAttribute("UserId") == null) {
			System.out.println("Must be logged in");
			JSONObject json = new JSONObject();
			try {
				json.put("Updated", false);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		}

		if (request.getParameter("Type") != null && request.getParameter("Type").equals("Favorite")) {

			int id = Integer.parseInt(session.getAttribute("UserId").toString());
			int gameId = Integer.parseInt(request.getParameter("GameId").toString());
			favoriteGame(id, gameId);

			JSONObject json = new JSONObject();
			try {
				json.put("Favorited", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("Report")) {

			int id = Integer.parseInt(session.getAttribute("UserId").toString());
			int gameId = Integer.parseInt(request.getParameter("GameId").toString());
			String report_reason = request.getParameter("Reason").toString();
			String report_details = request.getParameter("Details").toString();
			reportGame(id, gameId, report_reason, report_details);

			JSONObject json = new JSONObject();
			try {
				json.put("Reported", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("ReportBug")) {

			int id = Integer.parseInt(session.getAttribute("UserId").toString());
			int gameId = Integer.parseInt(request.getParameter("GameId").toString());
			int developerId = getGame(gameId).publisher_id;
			String bug_report_message = request.getParameter("BugInfo").toString();
			reportBug(id, developerId, gameId, bug_report_message);

			JSONObject json = new JSONObject();
			try {
				json.put("Reported", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("DeleteReport")) {
			System.out.println("Trying to delete...");

			System.out.println("ReportId is " + request.getParameter("ReportId"));
			int bugReportId = Integer.parseInt(request.getParameter("ReportId").toString());
			int userId = Integer.parseInt(session.getAttribute("UserId").toString());
			deleteBugReport(bugReportId, userId);

			JSONObject json = new JSONObject();
			try {
				json.put("ReportDeleted", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("RateReview")) {

			int id = Integer.parseInt(session.getAttribute("UserId").toString());
			int reviewId = Integer.parseInt(request.getParameter("ReviewId").toString());
			String rating = request.getParameter("Reason").toString();
			boolean useful = false;
			if (rating.equalsIgnoreCase("true"))
				useful = true;
			rateReview(id, reviewId, useful);

			JSONObject json = new JSONObject();
			try {
				json.put("Reported", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		}

		int id = Integer.parseInt(session.getAttribute("UserId").toString());
		String display_name = request.getParameter("Display_Name");
		String bio = request.getParameter("Bio");

		String data = request.getParameter("Image");
		
		if (data == null) {
			// Set data to non-null value
			// so placeholder file is still created for when the file is
			// modified
			data = "";
		}

		// Verify folder exists
		File dir = new File("/var/webapp/profile_pics");
		if (dir.exists() && dir.isDirectory()) {
			// continue
			System.out.println("Image Directory exists");
		} else {
			// Directory does not exist
			System.out.println("Create missing directory /var/webapp/profile_pics with write permissions");
		}

		FileOutputStream out = new FileOutputStream("/var/webapp/profile_pics/" + id, false);

		byte[] byteSet = data.getBytes();

		out.write(byteSet);
		out.close();

		UserProfile profile = new UserProfile(id, display_name, bio);

		updateProfile(profile);

		JSONObject json = new JSONObject();
		try {
			json.put("Updated", true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String jsonString = json.toString();
		response.setContentType("application/json");
		response.getWriter().write(jsonString);

	}

	void updateProfile(UserProfile profile) {
		System.out.println("Adding entry to games table");
		// Insert game info
		String query = "UPDATE Profiles SET display_name = \"" + profile.name + "\"," + " biography = \"" + profile.bio
				+ "\" " + "WHERE account_id LIKE " + profile.id;
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	UserProfile getProfile(int profileId) {
		Connection conn = null;
		// Perform action on specified id
		try {

			String sql = "SELECT * FROM Profiles WHERE account_id LIKE " + profileId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				System.out.println("Matching profile found");
				String display_name = rs.getString("display_name");
				String biography = rs.getString("biography");
				return new UserProfile(profileId, display_name, biography);
			} else {
				System.out.println("No match found for profile id " + profileId);
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	void favoriteGame(int accountId, int gameId) {
		System.out.println("Adding entry to favorites table");
		// Insert favorite game
		String query = "INSERT INTO favorites(account_id, game_id) VALUES (" + accountId + ", " + gameId + ")";
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	void reportGame(int accountId, int gameId, String report_reason, String report_details) {
		System.out.println("Adding entry to reports table");
		// Insert game report
		String query = "INSERT INTO reports(reporter_id, game_id, report_reason, report_details) VALUES (" + accountId
				+ ", " + gameId + ",\"" + report_reason + "\", \"" + report_details + "\")";
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	void reportBug(int reporterId, int developerId, int gameId, String bug_message) {
		System.out.println("Adding entry to bug reports table");
		// Insert game bug
		String query = "INSERT INTO bug_reports(reporter_id, developer_id, game_id, message) VALUES (" + reporterId
				+ ", " + developerId + ", " + gameId + ", \"" + bug_message + "\")";
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	void deleteBugReport(int bugReportId, int userId) {
		System.out.println("Removing entry from bug reports table");
		// Insert game info
		String query = "DELETE FROM bug_reports WHERE id = " + bugReportId + " AND developer_id = " + userId;
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	void rateReview(int accountId, int reviewId, boolean helpful) {
		System.out.println("Adding entry to review_votes table");
		// Insert game info
		String query = "INSERT INTO review_votes(review_Id, account_Id, useful) VALUES (" + reviewId + ", " + accountId
				+ "," + helpful + ")";
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	List<Review> getReviewsByReviewer(int reviewerId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<Review> Reviews = new ArrayList<Review>();
			String sql = "SELECT * FROM reviews WHERE reviewer_id = " + reviewerId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			while (rs.next()) {
				int reviewId = rs.getInt("id");
				int gameId = rs.getInt("game_id");
				String gameName = getGame(gameId).name;
				int accountId = rs.getInt("reviewer_id");
				String reviewerName = getDisplayName(conn, accountId);
				String title = rs.getString("review_title");
				String reviewDetails = rs.getString("review_description");
				int score = getReviewScore(reviewId);
				Review thing = new Review(reviewId, gameId, gameName, accountId, reviewerName, title, reviewDetails,
						score);
				Reviews.add(thing);
			}
			return Reviews;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	int getReviewScore(int reviewId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			String sql = "SELECT COUNT(*) FROM ( SELECT * FROM reviews AS r INNER JOIN review_votes AS rv ON r.id = rv.review_id AND rv.useful = 1 AND r.id = "
					+ reviewId + " ) x";
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			String sql2 = "SELECT COUNT(*) FROM ( SELECT * FROM reviews AS r INNER JOIN review_votes AS rv ON r.id = rv.review_id AND rv.useful = 0 AND r.id = "
					+ reviewId + " ) x";
			System.out.println("2nd Select is " + sql2);
			ResultSet rs2 = ConnectionHandler.getRS(conn, sql2);

			int upvotes = 0;
			int downvotes = 0;

			if (rs.next()) {
				// Only a single column is returned
				upvotes = rs.getInt(1);
			} else {
				System.out.println("No upvotes found");
			}

			if (rs2.next()) {
				// Only a single column is returned
				downvotes = rs.getInt(1);
			} else {
				System.out.println("No downvotes found");
			}

			// Return score (upvotes - downvotes)
			return upvotes - downvotes;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

	Game getGame(int gameId) {
		Connection conn = null;
		// Perform action on specified id
		try {

			String sql = "SELECT * FROM Games WHERE id LIKE " + gameId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs3 = ConnectionHandler.getRS(conn, sql);

			if (rs3.next()) {
				System.out.println("Matching game found");
				int game_id = gameId;
				String gameName = rs3.getString("title");
				String description = rs3.getString("description");
				int author = rs3.getInt("publisher_id");
				String author_name = getDisplayName(conn, author);
				String url = rs3.getString("url");
				return new Game(game_id, gameName, description, author_name, author, url);
			} else {
				System.out.println("No match found for game id " + gameId);
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	List<BugReport> getBugReportsByGameId(int game_Id) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<BugReport> bugReports = new ArrayList<BugReport>();
			String sql = "SELECT * FROM bug_reports WHERE game_id = " + game_Id;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			while (rs.next()) {
				int reportId = rs.getInt("id");
				int reporterId = rs.getInt("reporter_id");
				String reporter_name = getDisplayName(conn, reporterId);
				int gameId = rs.getInt("game_id");
				String game_name = getGame(gameId).name;
				String details = rs.getString("message");
				BugReport thing = new BugReport(reportId, reporterId, reporter_name, gameId, game_name, details);
				bugReports.add(thing);
			}
			return bugReports;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	List<BugReport> getBugReportsByPublisher(int accountId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<BugReport> bugReports = new ArrayList<BugReport>();
			String sql = "SELECT * FROM bug_reports AS b INNER JOIN Games AS g ON b.game_id = g.id AND g.publisher_id = "
					+ accountId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs10 = ConnectionHandler.getRS(conn, sql);

			while (rs10.next()) {
				int reportId = rs10.getInt("id");
				int reporterId = rs10.getInt("reporter_id");
				String reporter_name = getDisplayName(conn, reporterId);
				int gameId = rs10.getInt("game_id");
				String game_name = "";
				if (getGame(gameId).name != null) {
					game_name = getGame(gameId).name;
				}
				String details = rs10.getString("message");
				BugReport thing = new BugReport(reportId, reporterId, reporter_name, gameId, game_name, details);
				bugReports.add(thing);
				System.out.println("This is: " + thing.bug_info);
			}
			return bugReports;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	List<Game> getFavoriteGames(int accountId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<Game> Games = new ArrayList<Game>();
			String sql = "SELECT * FROM Games AS g INNER JOIN favorites AS fg ON g.id = fg.game_id AND fg.account_id = "
					+ accountId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs4 = ConnectionHandler.getRS(conn, sql);

			if (rs4 != null) {
				while (rs4.next()) {
					int gameId = rs4.getInt("id");
					String gameName = rs4.getString("title");
					String description = rs4.getString("description");
					int author = rs4.getInt("publisher_id");
					String author_name = getDisplayName(conn, author);
					String url = rs4.getString("url");
					Game thing = new Game(gameId, gameName, description, author_name, author, url);
					Games.add(thing);
				}
			}
			return Games;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	List<Game> getGamesByPublisher(int publisherId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<Game> Games = new ArrayList<Game>();
			String sql = "SELECT * FROM Games WHERE publisher_id=" + publisherId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs9 = ConnectionHandler.getRS(conn, sql);

			while (rs9.next()) {
				int gameId = rs9.getInt("id");
				String gameName = rs9.getString("title");
				String description = rs9.getString("description");
				int author = rs9.getInt("publisher_id");
				String author_name = getDisplayName(conn, author);
				String url = rs9.getString("url");
				Game thing = new Game(gameId, gameName, description, author_name, author, url);
				Games.add(thing);
			}
			return Games;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	String getDisplayName(Connection conn, int accountId) {
		try {
			String sql = "SELECT * FROM Profiles WHERE account_id LIKE " + accountId;
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				System.out.println("Matching game found");
				return rs.getString("display_name");
			} else {
				System.out.println("No match found for account id " + accountId);
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	List<UserProfile> getProfiles() {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<UserProfile> Profiles = new ArrayList<UserProfile>();
			String sql = "SELECT * FROM Profiles";
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);
			System.out.println(rs.toString());
			while (rs.next()) {
				String username = rs.getString("display_name");
				String bio = rs.getString("biography");
				int id = rs.getInt("account_id");
				UserProfile thing = new UserProfile(id, username, bio);
				Profiles.add(thing);
				System.out.println(thing);
			}
			return Profiles;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
	
	String getFileContents(String fileName) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		// All data is written on a single line
		String contents = "";
		try {
			contents = br.readLine();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br = null;
		}

		return contents;
	}
}
