package indie_u.auth;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("Username") != null) {
			System.out.println("You are already logged in as " + session.getAttribute("Username").toString());
			return;
		} else {
			System.out.println("You are not logged in.");
			System.out.println("Registering as bob");
		}
	}

	// Called by login controller when user hits submit button for login form
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		/**
		 * if (session.getAttribute("Username") != null) {
		 * System.out.println("You are already logged in as " +
		 * session.getAttribute("Username").toString()); return; } else {
		 * System.out.println("User is not logged in."); }
		 **/

		// Type indicates if request is for login or registration
		String requestType = request.getParameter("Type");

		if (requestType.equals("Register")) {
			String user = request.getParameter("Username");
			String pass = request.getParameter("Password");
			String email = request.getParameter("Email");

			System.out.println("Read in username " + user);
			System.out.println("Read in password " + pass);
			boolean successfulRegister = false;

			if (registerUser(user, pass, email)) {
				successfulRegister = true;
			} else {
				System.out.println("Login Failed");
			}

			JSONObject json = new JSONObject();
			try {
				json.put("Registered", successfulRegister);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);

		} else if (requestType.equals("Login")) {

			String user = request.getParameter("Username");
			String pass = request.getParameter("Password");

			System.out.println("Read in username " + user);
			System.out.println("Read in password " + pass);
			boolean isLoggedIn = false;
			int userId = -1;

			if (authenticateUser(user, pass)) {
				System.out.println("Successful login");
				session.setAttribute("Username", user);
				userId = getUserId(user);
				session.setAttribute("UserId", userId);
				isLoggedIn = true;
			} else {
				System.out.println("Login Failed");
			}

			JSONObject json = new JSONObject();
			try {
				json.put("LoggedIn", isLoggedIn);
				json.put("Name", user);
				json.put("Id", userId);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
		}
		else if (requestType.equals("Logout")){
			session.invalidate();
			System.out.println("Logged out.");
		}

		// doGet(request, response);
	}

	int getUserId(String username) {
		Connection conn = null;
		// Perform action on specified id
		try {
			conn = ConnectionHandler.getConnection();
			Statement stmt = conn.createStatement();

			String sql = "SELECT * FROM Accounts WHERE username LIKE \"" + username + "\"";
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				System.out.println("Matching account found");
				return rs.getInt("id");
			} else {
				System.out.println("Matching account not found for  " + username);
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return -1;
	}

	// Attempts to authenticate user using provided credentials
	// Returns true if login was successful
	// Returns false if login failed
	boolean authenticateUser(String username, String password) {
		System.out.println("Finding entry in Accounts table");
		Connection conn = null;
		// SELECT on username & password fields
		try {
			
			//Get salt that corresponds with user
			String sql = "SELECT salt FROM Accounts WHERE username LIKE \"" + username
					+ "\"";
			System.out.println(sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);
			
			byte[] salt = new byte[16];
			// Entry exists
			if (rs.next()) {
				//Get salt
				String saltString = rs.getString("salt");
				salt = DatatypeConverter.parseBase64Binary(saltString);
				String salt2 = DatatypeConverter.printBase64Binary(salt).toString();
				System.out.println(salt2);
			} else {
				return false;
			}
			
			// Create secure password
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			
			String secure_password = DatatypeConverter.printBase64Binary(hash);
			
			String sql2 = "SELECT username, password FROM Accounts WHERE username LIKE \"" + username
					+ "\" AND password LIKE \"" + secure_password + "\"";
			System.out.println(sql2);
			ResultSet rs2 = ConnectionHandler.getRS(conn, sql2);

			// Entry exists
			if (rs2.next()) {
				return true;
			} else {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

	// Attempts to create a user with the provided credentials
	// Returns true if account was created
	// Returns false if failed to create account
	boolean registerUser(String username, String password, String email) {
		System.out.println("Finding entry in Accounts table");
		Connection conn = null;
		// SELECT on username & email fields to make sure both are unique
		try {
			conn = ConnectionHandler.getConnection();
			Statement stmt = conn.createStatement();

			String sql = "SELECT username, email FROM Accounts WHERE username LIKE \"" + username
					+ "\" OR email LIKE \"" + email + "\"";
			ResultSet rs = stmt.executeQuery(sql);

			// Entry exists
			if (rs.next()) {
				System.out.println("User already exists in DB");
				return false;
			} else {
				// Username and email do not currently exist in DB
				addAccount(username, password, email);
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return false;
	}

	void addAccount(String username, String password, String email) {
		Connection conn = null;
		try {
			byte[] salt = new byte[16];
			SecureRandom random = new SecureRandom();
			random.nextBytes(salt);
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			System.out.printf("salt: %s%n", DatatypeConverter.printBase64Binary(salt));
			System.out.printf("hash: %s%n", DatatypeConverter.printBase64Binary(hash));
			String newPassword = DatatypeConverter.printBase64Binary(hash);
			String newSalt = DatatypeConverter.printBase64Binary(salt);
			// Create the account
			String query = "INSERT INTO Accounts (username, password, salt, email) VALUES (\"" + username + "\", \""
					+ newPassword + "\", \"" + newSalt + "\", \"" + email + "\")";
			System.out.println("Query is " + query);
			
			conn = ConnectionHandler.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.execute();

			// Get the id of the newly created account
			Statement stmt = conn.createStatement();

			String sql = "SELECT id FROM Accounts WHERE username LIKE \"" + username + "\"";
			ResultSet rs = stmt.executeQuery(sql);
			int id = -1;

			// Get id of entry
			if (rs.next()) {
				id = rs.getInt("id");
			} else {
				System.out.println("Failed to find matching entry after insertion");
				return;
			}

			// Create profile for the account
			String profileInsert = "INSERT INTO Profiles (account_id, display_name, biography)" + " VALUES (\"" + id
					+ "\", \"" + username + "\", \"\")";
			System.out.println("Profile insert is " + profileInsert);
			PreparedStatement preparedStatement2 = conn.prepareStatement(profileInsert);
			preparedStatement2.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}  finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
