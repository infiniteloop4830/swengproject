package indie_u.auth;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionHandler {

	static String url = "jdbc:mysql://ec2-54-68-246-181.us-west-2.compute.amazonaws.com:3306/indieDB";
	static String user = "newremoteuser";
	static String password = "password";
	static Connection con;

	public static Connection getConnection() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("JDBC Driver not found");
			e.printStackTrace();
		}

		Connection con = null;

		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			System.out.println("Error occurred while connecting");
			e.printStackTrace();
		}

		if (con == null) {
			System.out.println("Failed to get connection");
		}

		return con;

	}

	public static void executeStatement(String statement) {
		Connection connection = null;
		
		try {
			connection = getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(statement);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}  finally {
			if(connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static ResultSet getRS(Connection conn, String query) {
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			connection = conn;
			stmt = connection.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return rs;
	}
}
