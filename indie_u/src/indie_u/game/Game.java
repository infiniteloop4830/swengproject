package indie_u.game;

public class Game {

	public String name;
	public String description;
	public String publisher_name;
	public int publisher_id;
	public int game_id;
	public String url;

	public Game(String name, String description, String publisher_name, int publisher_id, String url) {
		this.name = name;
		this.description = description;
		this.publisher_name = publisher_name;
		this.publisher_id = publisher_id;
		this.url = url;
	}

	public Game(int game_id, String name, String description, String publisher_name, int publisher_id, String url) {
		this.name = name;
		this.description = description;
		this.publisher_name = publisher_name;
		this.publisher_id = publisher_id;
		this.url = url;
		this.game_id = game_id;
	}
}
