package indie_u.game;

public class Review {
	// Primary key for review
	public int id;
	// The id of the game the review is associated with
	public int gameId;
	public String gameName;
	// Id of the account under which the review was created
	public int accountId;
	public String reviewerName;

	// Short title of review
	// Ex: Only if you like Diablo 3
	public String title;
	// Details of Review
	// Ex: It was alright considering how much time I got out of it.
	// Pros: Good game, fun on first playthrough
	// Cons: Buggy multiplayer, no trading, grindy
	public String reviewDetails;

	// Score is based on up and downvotes of review
	public int score;

	public Review(int id, int gameId, String gameName, int accountId, String reviewerName, String title,
			String reviewDetails, int score) {
		this.id = id;
		this.gameId = gameId;
		this.gameName = gameName;
		this.accountId = accountId;
		this.reviewerName = reviewerName;
		this.title = title;
		this.reviewDetails = reviewDetails;
		this.score = score;
	}
}
