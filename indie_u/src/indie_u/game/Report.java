package indie_u.game;

public class Report {

	public int id;
	public int reporterId;
	public String reporterName;
	public int gameId;
	public String gameName;
	public String reason;
	public String report_details;

	public Report(int reporterId, String reporterName, int gameId, String gameName, String reason,
			String report_details) {
		this.reporterId = reporterId;
		this.reporterName = reporterName;
		this.gameId = gameId;
		this.gameName = gameName;
		this.reason = reason;
		this.report_details = report_details;
	}
}
