package indie_u.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import indie_u.auth.ConnectionHandler;

@WebServlet("/GameFormServlet")
public class GameFormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public GameFormServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Get current game data
		if (request.getParameter("Id") == null) {
			System.out.println("Invalid game Id");
			return;
		}
		int id = Integer.parseInt(request.getParameter("Id"));

		if (request.getParameter("Type") != null && request.getParameter("Type").equals("Download")) {

			// Find file with matching idea and set contents to data
			String data = getFileContents("/var/webapp/uploads/" + id);
			
			JSONObject json = new JSONObject();
			try {
				json.put("downloaded", true);
				json.put("data", data);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		}

		if (request.getParameter("Type") != null && request.getParameter("Type").equals("All")
				&& request.getParameter("Id").equals("0")) {

			List<Game> foundgames = getGames();
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (Game item : foundgames) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("game_id", item.game_id);
					anotherone.put("title", item.name);
					anotherone.put("description", item.description);
					anotherone.put("publisher", item.publisher_name);
					anotherone.put("url", item.url);
					anotherone.put("id", item.publisher_id);
					array1.put(anotherone);
				}
				gamelist.put("Games", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("GetReview")) {

			List<Review> reviews = getReviews(id);
			JSONObject gamelist = new JSONObject();
			try {
				JSONArray array1 = new JSONArray();
				for (Review item : reviews) {
					JSONObject anotherone = new JSONObject();
					anotherone.put("id", item.id);
					anotherone.put("account_id", item.accountId);
					anotherone.put("reviewer_name", item.reviewerName);
					anotherone.put("game_id", item.gameId);
					anotherone.put("game_name", item.gameName);
					anotherone.put("title", item.title);
					anotherone.put("review_details", item.reviewDetails);
					anotherone.put("score", item.score);
					array1.put(anotherone);
				}
				gamelist.put("Reviews", array1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String games = gamelist.toString();
			response.setContentType("application/json");
			response.getWriter().write(games);
			// System.out.println(gamelist);

			return;
		}

		Game game = getGame(id);

		if (game == null) {
			System.out.println("No Game found");
			return;
		}

		JSONObject json = new JSONObject();
		try {
			json.put("title", game.name);
			json.put("description", game.description);
			json.put("publisher", game.publisher_name);
			json.put("game_id", game.game_id);
			json.put("author", game.publisher_id);
			json.put("url", game.url);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String jsonString = json.toString();
		response.setContentType("application/json");
		response.getWriter().write(jsonString);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		if (session.getAttribute("UserId") == null) {
			System.out.println("Must be logged in");
			JSONObject json = new JSONObject();
			try {
				json.put("Uploaded", false);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		}

		String title = request.getParameter("Title");
		String description = request.getParameter("Description");
		int author_id = Integer.parseInt(session.getAttribute("UserId").toString());
		String payment_info = request.getParameter("PaymentInfo");
		System.out.println("Current id found to be " + author_id);

		Game newGame = new Game(title, description, "UNUSED 1", author_id, payment_info);

		if (request.getParameter("Type") != null && request.getParameter("Type").equals("UpdateGame")) {
			// Pass game id in order to update existing entry
			int game_id = Integer.parseInt(request.getParameter("GameId").toString());
			
			if(author_id != getGame(game_id).publisher_id) {
				System.out.println(author_id + " attempted to update a game they are not a publisher for.");
				
				JSONObject json = new JSONObject();
				try {
					json.put("Updated", false);
				} catch (Exception e) {
					e.printStackTrace();
				}

				String jsonString = json.toString();
				response.setContentType("application/json");
				response.getWriter().write(jsonString);

				return;
			}
			
			String game_title = request.getParameter("Title");
			String game_description = request.getParameter("Description");
			String game_payment_info = request.getParameter("PaymentInfo");
			Game updatedGame = new Game(game_id, game_title, game_description, "UNUSED 2", author_id, game_payment_info);

			updateGameInfo(updatedGame);

			String data = request.getParameter("File");
			
			if (data == null) {
				// Don't overwrite existing file
				System.out.println("No new file provided");
			} else {
				File dir = new File("/var/webapp/uploads");
				if (dir.exists() && dir.isDirectory()) {
					// continue
					System.out.println("Directory exists");
				} else {
					// Directory does not exist
					System.out.println("Create missing directory /var/webapp/uploads with write permissions");
				}

				FileOutputStream out = new FileOutputStream("/var/webapp/uploads/" + game_id, false);

				byte[] byteSet = data.getBytes();

				out.write(byteSet);
				out.close();
			}
			
			JSONObject json = new JSONObject();
			try {
				json.put("Updated", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);

			return;
		} else if (request.getParameter("Type") != null && request.getParameter("Type").equals("Review")) {

			int id = Integer.parseInt(session.getAttribute("UserId").toString());
			int gameId = Integer.parseInt(request.getParameter("GameId").toString());
			int good_game = Integer.parseInt(request.getParameter("Score"));
			String report_reason = request.getParameter("Title").toString();
			String report_details = request.getParameter("Review").toString();
			reviewGame(id, gameId, good_game, report_reason, report_details);

			JSONObject json = new JSONObject();
			try {
				json.put("Reported", true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String jsonString = json.toString();
			response.setContentType("application/json");
			response.getWriter().write(jsonString);
			return;
		}

		int id = addGame(newGame);

		String data = request.getParameter("File");
		
		if (data == null) {
			// Set data to non-null value
			// so placeholder file is still created for when the file is
			// modified
			data = "";
		}

		// Verify folder exists
		File dir = new File("/var/webapp/uploads");
		if (dir.exists() && dir.isDirectory()) {
			// continue
			System.out.println("Directory exists");
		} else {
			// Directory does not exist
			System.out.println("Create missing directory /var/webapp/uploads with write permissions");
		}

		FileOutputStream out = new FileOutputStream("/var/webapp/uploads/" + id, false);

		byte[] byteSet = data.getBytes();

		out.write(byteSet);
		out.close();

		JSONObject json = new JSONObject();
		try {
			json.put("Uploaded", true);
			json.put("Title", title);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String jsonString = json.toString();
		response.setContentType("application/json");
		response.getWriter().write(jsonString);
	}

	String getFileContents(String fileName) throws FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		// All data is written on a single line
		String contents = "";
		try {
			contents = br.readLine();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br = null;
		}

		return contents;
	}

	int addGame(Game game) {
		System.out.println("Adding entry to games table");
		Connection conn = null;
		// Insert game info
		try {
			String query = "INSERT INTO Games (title, description, publisher_id, payment_info) VALUES (\"" + game.name + "\", \""
					+ game.description + "\", \"" + game.publisher_id + "\", \"" + game.url + "\")";
			System.out.println("Query is " + query);
			conn = ConnectionHandler.getConnection();
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.execute();

			String getId = "SELECT LAST_INSERT_ID()";
			ResultSet rs = ConnectionHandler.getRS(conn, getId);

			if (rs.next()) {
				System.out.println("Id found");
				System.out.println("Id is " + rs.getInt(1));
				return rs.getInt(1);
			} else {
				System.out.println("No match found for new record");
				return -1;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return -1;
	}

	void updateGameInfo(Game game) {
		System.out.println("Updating entry in games table");
		// Update game info
		String query = "UPDATE Games SET title = \"" + game.name + "\"," + " description = \"" + game.description
				+ "\", " + " payment_info = \"" + game.url + "\" " + "WHERE id = " + game.game_id;
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	String getDisplayName(Connection conn, int accountId) {
		String displayName = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {

			String sql = "SELECT * FROM Profiles WHERE account_id LIKE " + accountId;
			rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				System.out.println("Matching game found");
				displayName = rs.getString("display_name");
			} else {
				System.out.println("No match found for account id " + accountId);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return displayName;
	}

	Game getGame(int gameId) {
		Connection conn = null;
		// Perform action on specified id
		try {

			String sql = "SELECT * FROM Games WHERE id LIKE " + gameId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			if (rs.next()) {
				System.out.println("Matching game found");
				int game_id = gameId;
				String gameName = rs.getString("title");
				String description = rs.getString("description");
				int author = rs.getInt("publisher_id");
				String author_name = getDisplayName(conn, author);
				String url = rs.getString("payment_info");
				return new Game(game_id, gameName, description, author_name, author, url);
			} else {
				System.out.println("No match found for game id " + gameId);
				return null;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	void reviewGame(int accountId, int gameId, int good_game, String review_title, String review_description) {
		System.out.println("Adding entry to reviews table...");
		// Insert game info
		String query = "INSERT INTO reviews(reviewer_id, game_id, good_game, review_title, review_description) VALUES ("
				+ accountId + ", " + gameId + ", " + good_game + ",\"" + review_title + "\", \"" + review_description
				+ "\")";
		System.out.println("Query is " + query);
		ConnectionHandler.executeStatement(query);
	}

	List<Review> getReviews(int gameId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			List<Review> Reviews = new ArrayList<Review>();
			String sql = "SELECT * FROM reviews WHERE game_id = " + gameId;
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			while (rs.next()) {
				// System.out.println("Reviews found!");
				int reviewId = rs.getInt("id");
				int reviewer_id = rs.getInt("reviewer_id");
				String reviewer_name = getDisplayName(conn, reviewer_id);
				int game_id = rs.getInt("game_id");
				String game_name = getGame(gameId).name;
				String review_title = rs.getString("review_title");
				String review_description = rs.getString("review_description");
				int score = getReviewScore(reviewId);
				Review thing = new Review(reviewId, game_id, game_name, reviewer_id, reviewer_name, review_title,
						review_description, score);
				Reviews.add(thing);
			}
			return Reviews;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	int getReviewScore(int reviewId) {
		Connection conn = null;
		// Perform action on specified id
		try {
			String sql = "SELECT COUNT(*) FROM ( SELECT * FROM reviews AS r INNER JOIN review_votes AS rv ON r.id = rv.review_id AND rv.useful = 1 AND r.id = "
					+ reviewId + " ) x";
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			String sql2 = "SELECT COUNT(*) FROM ( SELECT * FROM reviews AS r INNER JOIN review_votes AS rv ON r.id = rv.review_id AND rv.useful = 0 AND r.id = "
					+ reviewId + " ) x";
			System.out.println("2nd Select is " + sql2);
			ResultSet rs2 = ConnectionHandler.getRS(conn, sql2);

			int upvotes = 0;
			int downvotes = 0;

			if (rs.next()) {
				// Only a single column is returned
				upvotes = rs.getInt(1);
			} else {
				System.out.println("No upvotes found");
			}

			if (rs2.next()) {
				// Only a single column is returned
				downvotes = rs2.getInt(1);
			} else {
				System.out.println("No downvotes found");
			}

			// Return score (upvotes - downvotes)
			return upvotes - downvotes;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

	List<Game> getGames() {
		List<Game> games = null;
		Connection conn = null;

		// Perform action on specified id
		try {
			games = new ArrayList<Game>();
			String sql = "SELECT * FROM Games";
			System.out.println("Select is " + sql);
			conn = ConnectionHandler.getConnection();
			ResultSet rs = ConnectionHandler.getRS(conn, sql);

			while (rs.next()) {
				// System.out.println("Games found!");
				int gameId = rs.getInt("id");
				String gameName = rs.getString("title");
				String description = rs.getString("description");
				int author = rs.getInt("publisher_id");
				String author_name = getDisplayName(conn, author);
				String url = rs.getString("url");
				Game thing = new Game(gameId, gameName, description, author_name, author, url);
				games.add(thing);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return games;
	}
}
