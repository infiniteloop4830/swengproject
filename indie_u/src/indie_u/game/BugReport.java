package indie_u.game;

public class BugReport {

	public int id;
	public int reporterId;
	public String reporterName;
	public int gameId;
	public String gameName;
	public String bug_info;
	
	public BugReport(int id, int reporterId, String reporterName, int gameId, String gameName, String bug_info){
		this.id = id;
		this.reporterId = reporterId;
		this.reporterName = reporterName;
		this.gameId = gameId;
		this.gameName = gameName;
		this.bug_info = bug_info;
	}
}
