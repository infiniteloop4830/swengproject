angular.module("myapp.controllers", ['ngCookies']).controller("myappcontroller",
		function($scope, $http, $location, $cookies, $window) {	
			$scope.username = $cookies.username;
			$scope.userId = $cookies.userId;
			console.log("Username is " + $scope.username);
			console.log("User id is " + $scope.userId);
	
			$scope.loginWithServer = function() {
				console.log("Login");
				
				$http({
					method : 'POST',
					url : 'LoginServlet',
					params : {
						Type : 'Login',
						Username : $scope.login.name,
						Password : $scope.login.pass
					}
				}).success(function(response) {
					console.log($scope.login.name);
					console.log(response.LoggedIn);
					console.log(response.Name);
					if (response.LoggedIn == false) {
						console.log("Not Logged In");
					} else if (response.LoggedIn == true) {
						console.log("Logged in as " + response.Name);
						$cookies.username = response.Name;
						$cookies.userId = response.Id;
						$window.location.reload();
					}
				}).error(function(response) {
					console.log(response);
				});

			};

			$scope.logoutWithServer = function(){
				$http({
					method : 'POST',
					url : 'LoginServlet',
					params : {
						Type : 'Logout',
					}
				}).success(function(response) {
					console.log("Logged out.");
					delete $cookies.username;
					delete $cookies.userId;
					$window.location.href ="index.html";
				});
			}
			
			$scope.registerWithServer = function() {
				console.log("Register");
				
				$http({
					method : 'POST',
					url : 'LoginServlet',
					params : {
						Type : 'Register',
						Username : $scope.register.name,
						Password : $scope.register.pass,
						Email : $scope.register.email
					}
				}).success(function(response) {
					if (response.Registered == false) {
						console.log("Failed to Register");
					} else if (response.Registered == true) {
						console.log("New account registered!");
						$window.location.reload();
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getReviews = function(gameId){
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				console.log("Get all reviews...");
				$http({
					method:'GET',
					url:'GameFormServlet',
					params : {
						Type:'GetReview',
						 Id: gameId
					}
				}).success(function(response) {
					$scope.reviews = [];
					if(response.Reviews.length != null) {
						for(var i=0;i<response.Reviews.length;i++){
							$scope.reviews.push(response.Reviews[i]);
						}
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getReviewsByReviewer = function(userId){
				if(!userId) {
					if($location.search().id) {
						userId = $location.search().id;
					} else {
						userId = $scope.userId;
					}
				}
				
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type:'GetReviews',
						 Id: userId
					}
				}).success(function(response) {
					$scope.reviews = [];
					for(var i=0;i<response.Reviews.length;i++){
						$scope.reviews.push(response.Reviews[i]);
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.review = function(gameId) {
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						gameId = 1;
					}
				}
				$http({
					method : 'POST',
					url : 'GameFormServlet',
					params : {
						Type : 'Review',
						GameId: gameId,
						Title: $scope.review.title,
						Score: $scope.review.score,
						Review: $scope.review.review
					}
				}).success(function(response) {
					$window.location.reload();
				}).error(function(response) {
					console.log(response);
				});

			};
			
			
			
			$scope.getGames = function(){
				console.log("Get all games...");
				$http({
					method:'GET',
					url:'GameFormServlet',
					params : {
						Type:'All',
						 Id: 0
					}
				}).success(function(response) {
					$scope.games = [];
					for(var i=0;i<response.Games.length;i++){
						$scope.games.push(response.Games[i]);
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.search = function(){
				
				$http({
					method:'GET',
					url: 'SearchServlet',
					params :{ 
						Type :'gsearch',
						searched: $scope.search.query
					}
				}).success(function(response) {
					$scope.games = [];
					if(response.Games != null) {
						for(var i=0; i<response.Games.length; i++){
							$scope.games.push(response.Games[i]);
						}
					}
					
				}).error(function(response){
					console.log(response);
				});
				
				$http({
					method: 'GET',
					url: 'SearchServlet',
					params :{
						Type : 'psearch',
						searched : $scope.search.query
					}
				}).success(function(response){
					$scope.profiles = [];
					if(response.Profiles != null) {
						for(var i=0;i<response.Profiles.length;i++){
							$scope.profiles.push(response.Profiles[i]);
						}
					}
					
				}).error(function(response) {
					console.log(response);
				});
				
			}
			
			$scope.getGamesByPublisher = function(userId){
				if(!userId) {
					if($location.search().id) {
						userId = $location.search().id;
					} else {
						userId = $scope.userId;
					}
				}
				
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type:'AllByPublisher',
						 Id: userId
					}
				}).success(function(response) {
					$scope.games = [];
					for(var i=0;i<response.Games.length;i++){
						$scope.games.push(response.Games[i]);
					}
					
				}).error(function(response) {
					console.log(response);
				});
				
			};
			
			$scope.rateReview = function(revId,good_game){
				if(!revId) {
					if($location.search().id) {
						revId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						revId = 1;
					}
				}
				
				$http({
					method:'POST',
					url:'ProfileServlet',
					params : {
						Type: "RateReview",
						ReviewId: revId,
						UserId: $scope.userId,
						Reason: good_game
					}
				}).success(function(response) {
					$window.location.reload();
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getProfiles = function(){
				console.log("Get all profiles...");
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type:'All',
						 Id: 0
					}
				}).success(function(response) {
					$scope.profiles = [];
					for(var i=0;i<response.Profiles.length;i++){
						$scope.profiles.push(response.Profiles[i]);
					}
					
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.report = function(gameId){
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				console.log("Reporting...");
				$http({
					method:'POST',
					url:'ProfileServlet',
					params : {
						Type: "Report",
						GameId: gameId,
						Reason: $scope.report.subject,
						Details: $scope.report.info
					}
				}).success(function(response) {
					$window.location.reload();
				}).error(function(response) {
					console.log(response);
				});

			};
			$scope.reportBug = function(gameId){
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				console.log("Reporting bug...");
				$http({
					method:'POST',
					url:'ProfileServlet',
					params : {
						Type:"ReportBug",
						GameId: gameId,
						Reason: $scope.report.reason,
						BugInfo: $scope.report.details
					}
				}).success(function(response) {
					$window.location.reload();
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getAllReports = function(userId){
				if(!userId) {
					if($location.search().id) {
						userId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						userId = $scope.userId;
					}
				}
				console.log("Get reports for publisher...");
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type:'GetAllBugReports',
						 Id: userId
					}
				}).success(function(response) {
					$scope.allReports = [];
					if(response.BugReports != null) {
						for(var i=0;i<response.BugReports.length;i++){
							$scope.allReports.push(response.BugReports[i]);
						}
					}
					
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getGameReports = function(gameId){
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				console.log("Get reports for game: " + gameId);
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type: "GetGameBugReports",
						GameId: gameId,
						Id: $scope.userId
					}
				}).success(function(response) {
					$scope.gameReports = [];
					if(response.BugReports.length != null) {
						for(var i=0;i<response.BugReports.length;i++){
							$scope.gameReports.push(response.BugReports[i]);
						}
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.deleteReport = function(userId, repId){
				if(!userId) {
					if($location.search().id) {
						userId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						userId = $scope.userId;
					}
				}
				console.log("DELETE report...");
				$http({
					method:'POST',
					url:'ProfileServlet',
					params : {
						Type: "DeleteReport",
						Id: userId,
						ReportId: repId
					}
				}).success(function(response) {
					console.log("Deleted report " + $scope.deleteBug.number);
					$window.location.reload();
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.favoriteGame = function(gameId){
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				$http({
					method:'POST',
					url:'ProfileServlet',
					params : {
						Type:'Favorite',
						 GameId: gameId
					}
				}).success(function(response) {
					//console.log("Retrieved games " + response.Games[3].title);
				}).error(function(response) {
					console.log(response);
				});

			};
			
			
			$scope.getFavoriteGames = function(userId){
				if(!userId) {
					if($location.search().id) {
						userId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						userId = $scope.userId;
					}
				}
				$http({
					method:'GET',
					url:'ProfileServlet',
					params : {
						Type:'GetFavoriteGames',
						Id: userId
					}
				}).success(function(response) {
					$scope.favgames = [];
					if(response.Games != null) {
						for(var i=0;i<response.Games.length;i++){
							$scope.favgames.push(response.Games[i]);
						}
					}
					
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.getGame = function(gameId) {
				console.log("Get Game with Id " + gameId);
				//Ex path http://localhost:8080/indie_u/ViewGame.html#?id=5
				console.log("Location id is " + $location.search().id);
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				
				$http({
					method : 'GET',
					url : 'GameFormServlet',
					params : {
						Id : gameId
					}
				}).success(function(response) {
					console.log("Retrieved game " + response.title);
					console.log(response.title + " " + response.description + " " + response.publisher+" "+response.author);
					$scope.game ={
							id: gameId,
							title: response.title,
							description: response.description,
							publisher: response.publisher,
							author: response.author,
							url: response.url
					};
					console.log($scope.game.title);
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.uploadGame = function() {
				console.log("Upload Game");
				
				$http({
					method : 'POST',
					url : 'GameFormServlet',
					params : {
						Title : $scope.upload.title,
						Description : $scope.upload.description,
						File : $scope.upload.file
					}
				}).success(function(response) {
					console.log("Uploading game " + $scope.upload.title);
					if (response.Uploaded == false) {
						console.log("Upload Failed: Must be logged in");
					} else if (response.Uploaded == true) {
						console.log("Uploaded Game: " + response.Title);
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			getContents = function(string) {
			    var array = string.split(':');
			    array = array[1].split(';');
			    return array[0];
			};
			
			getData = function(string) {
				var array = string.split(';');
			    array = array[1].split(',');
			    return array[1];
			};
			
			$scope.downloadGame = function() {
				console.log("Download Game");
				
				$http({
					method : 'GET',
					url : 'GameFormServlet',
					params : {
						Type: 'Download', 
						Id : $scope.game.id
					}
				}).success(function(response) {
					console.log("Downloading game " + $scope.game.title);
					if (response.downloaded == false) {
						console.log("Download Failed: Must be logged in");
					} else if (response.downloaded == true) {
						console.log("Successfully Downloaded: " + response.Title);
						
						console.log("Reading sent data");
						
						var data = response.data;
						var b64Data = getData(data);
						var contentType = getContents(data);
						
						var blob = b64toBlob(b64Data, contentType);
						
						var downloadLink = angular.element('<a></a>');
                        downloadLink.attr('href',window.URL.createObjectURL(blob));
                        downloadLink.attr('download', $scope.game.title);
                        downloadLink[0].click();
						
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			function b64toBlob(b64Data, contentType, sliceSize) {
				  contentType = contentType || '';
				  sliceSize = sliceSize || 512;

				  var byteCharacters = atob(b64Data);
				  var byteArrays = [];

				  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
				    var slice = byteCharacters.slice(offset, offset + sliceSize);

				    var byteNumbers = new Array(slice.length);
				    for (var i = 0; i < slice.length; i++) {
				      byteNumbers[i] = slice.charCodeAt(i);
				    }

				    var byteArray = new Uint8Array(byteNumbers);

				    byteArrays.push(byteArray);
				  }

				  var blob = new Blob(byteArrays, {type: contentType});
				  return blob;
				};
			
			$scope.getProfile = function(profileId) {
				console.log("Get Profile with Id " + profileId);
				//Ex path http://localhost:8080/indie_u/EditProfile.html#?id=5
				console.log("Location id is " + $location.search().id);
				if(!profileId) {
					if($location.search().id) {
						profileId = $location.search().id;
					} else {
						profileId = $cookies.userId;
					}
				}
				
				$http({
					method : 'GET',
					url : 'ProfileServlet',
					params : {
						Id : profileId
					}
				}).success(function(response) {
					console.log("Retrieved profile " + response.Profile);
					console.log(response.id + " " + response.name + " " + response.bio);
					console.log("Reading sent data");
					
					var data = response.data;
					
					$scope.profile = {
							id: response.id,
							name: response.name,
							bio: response.bio,
							image: data
					};
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.updateProfile = function() {
				console.log("Update Profile");
				
				$http({
					method : 'POST',
					url : 'ProfileServlet',
					params : {
						Display_Name : $scope.profile.name,
						Bio : $scope.profile.bio,
						Image : $scope.profile.image
					}
				}).success(function(response) {
					console.log("Updating Profile");
					if (response.Updated == false) {
						console.log("Update Failed: Must be logged in");
					} else if (response.Updated == true) {
						console.log("Successful Update");
						$window.location.reload();
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.updateGame = function(gameId) {
				console.log("Update game...");
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				$http({
					method : 'POST',
					url : 'GameFormServlet',
					params : {
						Type: "UpdateGame",
						GameId : gameId,
						Title: $scope.game.title,
						Description : $scope.game.description,
						PaymentInfo: $scope.game.url,
						File: $scope.update.file
					}
				}).success(function(response) {
					console.log("Updating Game...");
					if (response.Updated == false) {
						console.log("Update Failed: Must be logged in");
					} else if (response.Updated == true) {
						console.log("Successful Update");
						$window.location.reload();
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			$scope.updateGameFile = function(gameId) {
				console.log("Update game file...");
				if(!gameId) {
					if($location.search().id) {
						gameId = $location.search().id;
					} else {
						//TODO display error page of invalid gameId or redirect to home
						gameId = 1;
					}
				}
				$http({
					method : 'POST',
					url : 'GameFormServlet',
					params : {
						Type: "UpdateGameFile",
						GameId : gameId,
						File: $scope.update.file
					}
				}).success(function(response) {
					console.log("Updating Game File...");
					if (response.Updated == false) {
						console.log("Update Failed");
					} else if (response.Updated == true) {
						console.log("Successful Update");
						$window.location.reload();
					}
				}).error(function(response) {
					console.log(response);
				});

			};
			
			
			// Initialization
			$scope.init = function() {
				// On init, set profile id to current user's id to populate
				// edit profile fields with current values
				// TODO resolve usage of rootScope variables to be defined across all scopes
				console.log("User id is currently " + $cookies.userId);
				
				
				$scope.profile = {
						id: $cookies.userId
				};
				console.log("Going to set " + $scope.profile.id);
				$scope.getProfile($scope.profile.id);
			};
			
		}).directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);