# Create & Use Indie DB
CREATE DATABASE indieDB;
USE indieDB;

# Create Accounts Table
CREATE TABLE Accounts (
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(128) NOT NULL,
	password VARCHAR(128) NOT NULL,
	salt VARCHAR(128) NOT NULL,
	email VARCHAR(128) NOT NULL,
	PRIMARY KEY(id)
);

# Add Account to Table
INSERT INTO Accounts (username, hashed_password, email) 
	VALUES ( "test_user", "password", "test@test.net");
	
# Create Games Table
CREATE TABLE Games (
	id INT NOT NULL AUTO_INCREMENT,
	publisher_id INT NOT NULL,
	title VARCHAR(128) NOT NULL,
	description VARCHAR(4096),
	url VARCHAR(255),
	payment_info VARCHAR(4096),
	PRIMARY KEY (id),
	FOREIGN KEY (publisher_id) REFERENCES Accounts(id)
);

# Add Game to Table
INSERT INTO Games (title, description, url, publisher_id) 
	VALUES ( "Test Game", "Sample Text", "www.google.com", 1);

# Create Profiles Table
CREATE TABLE Profiles (
	account_id INT NOT NULL,
	display_name VARCHAR(128) NOT NULL,
	biography VARCHAR(4096) NOT NULL,
	PRIMARY KEY(account_id),
	FOREIGN KEY (account_id) REFERENCES Accounts(id)
);

# Add Profile to Table
# Should be performed immediately after account creation
INSERT INTO Profiles (account_id, display_name, biography) 
	VALUES (1, "Another One", "Another one... Another one... Another one...");

# Review Table
CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	reviewer_id INT NOT NULL,
	game_id INT NOT NULL,
	good_game BOOLEAN,
	review_title VARCHAR(128) NOT NULL,
	review_description VARCHAR(4096) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (reviewer_id) REFERENCES Accounts(id),
	FOREIGN KEY (game_id) REFERENCES Games(id)
);

# Example Review
INSERT INTO reviews (reviewer_id, game_id, good_game, review_title, review_description)
	VALUES (2, 10, TRUE, "It was alright", "I recommend it to anyone who genuinely enjoys classic diablo");
	
# Report Table
# Indicates report for a game that is malicious or otherwise should not be on the site
CREATE TABLE reports (
	id INT NOT NULL AUTO_INCREMENT,
	reporter_id INT NOT NULL,
	game_id INT NOT NULL,
	report_reason VARCHAR(128) NOT NULL,
	report_details VARCHAR(4096) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (reporter_id) REFERENCES Accounts(id),
	FOREIGN KEY (game_id) REFERENCES Games(id)
);

# Example Report
INSERT INTO reports (reporter_id, game_id, report_reason, report_details)
	VALUES (2, 10, "Breaks ToS", "Contains adware");

# Bug Reports Table for Game Developers
CREATE TABLE bug_reports (
	id INT NOT NULL AUTO_INCREMENT,
	reporter_id INT NOT NULL,
	developer_id INT NOT NULL,
	game_id INT NOT NULL,
	message VARCHAR(4096) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (reporter_id) REFERENCES Accounts(id),
	FOREIGN KEY (developer_id) REFERENCES Accounts(id),
	FOREIGN KEY (game_id) REFERENCES Games(id)
);

# Example Bug Report
INSERT INTO bug_reports (reporter_id, developer_id, game_id, message)
	VALUES (2, 3, 11, "Game crashes on startup with error code 3BJ-1000 regardless of whether or not I run as admin");

# Favorited Games Table
CREATE TABLE favorites (
	account_id INT NOT NULL,
	game_id INT NOT NULL,
	FOREIGN KEY (account_id) REFERENCES Accounts(id),
	FOREIGN KEY (game_id) REFERENCES Games(id)
);

# Example Favorite
INSERT INTO favorites (account_id, game_id)
	VALUES (2, 10);

# Get all favorite games of a specific account id
SELECT * FROM Games AS g 
	INNER JOIN favorites AS fg
	ON g.id = fg.game_id AND fg.account_id = 3;
	
# Review Votes Table
CREATE TABLE review_votes (
	review_id INT NOT NULL,
	account_id INT NOT NULL,
	useful BOOLEAN NOT NULL,
	FOREIGN KEY (review_id) REFERENCES reviews(id),
	FOREIGN KEY (account_id) REFERENCES Accounts(id)
);

# Get count of helpful votes on review
SELECT COUNT(*) FROM ( SELECT * FROM reviews AS r INNER JOIN review_votes AS rv ON r.id = rv.review_id AND rv.useful = 1 ) x;

